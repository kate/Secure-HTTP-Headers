# Secure-HTTP-Headers

The Secure HTTP or CSP Generator is a program to help you find the right command for your server configuration. Currently the program is only available for the Apache web server. 

This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
Know what you're doing before using this program!

# Fonts used in this project:
- Roboto: https://fonts.google.com/specimen/Roboto (Apache License)
- Zen+Dots: https://fonts.google.com/specimen/Zen+Dots (Open Font License)

